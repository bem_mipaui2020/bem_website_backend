const { Client } = require("pg");
var bcrypt = require("bcryptjs");
var dateFormat = require("dateformat");

const client = new Client({
  user: "muhammadalbar",
  password: "Muhammad123",
  host: "localhost",
  port: 5432,
  database: "bem_mipa",

  // user: process.env.PG_USER,
  // password: process.env.PG_PASSWORD,
  // host: process.env.PG_HOST,
  // port: process.env.PG_PORT,
  // database: process.env.PG_DATABASE,
  // ssl: true,
});
// console.log(client)
client.connect((err) => {
  if (err) {
    throw err;
  }
  let createUserTable = `CREATE TABLE IF NOT EXISTS USERS(id_user SERIAL not null primary key,
        username varchar(200) unique not null,
        password varchar(200) unique not null
        );`;

  let createArticleTable = `CREATE TABLE IF NOT EXISTS ARTICLE(id_article SERIAL not null primary key,
        title varchar(100) not null,
        writer varchar(100) not null,
        content text not null,
        thumbnail_photo varchar(500) not null,
        created_date timestamp not null
        );`;

  let createAdkesmaTable = `CREATE TABLE IF NOT EXISTS ADKESMA(id SERIAL not null primary key,
        caption varchar(300) not null,
        url varchar(100) not null
        );`;

  client.query(createUserTable, (err, results, fields) => {
    if (err) {
      throw err;
    }
    var val = [];
    var setValue = (value) => {
      val = value;
    };
    let query = `SELECT * FROM USERS;`;
    client.query(query, (err, results) => {
      if (err) {
        throw err;
      }
      setValue(results);
      var string = JSON.stringify(val);
      var users = JSON.parse(string);

      if (users.rowCount === 0) {
        var hashedPassword = bcrypt.hashSync("admin123");
        let createUser = `INSERT INTO USERS(username, password) 
                VALUES ('admin','${hashedPassword}');`;
        client.query(createUser, function (error, results) {
          if (error) {
            console.log(error);
          }
        });
      }
    });
  });

  client.query(createArticleTable, (err, results, fields) => {
    if (err) {
      throw err;
    }
    var val = [];
    var setValue = (value) => {
      val = value;
    };
    let query = `SELECT * FROM ARTICLE;`;
    client.query(query, (err, results) => {
      if (err) {
        throw err;
      }
      setValue(results);
      var string = JSON.stringify(val);
      var articles = JSON.parse(string);

      if (articles.rowCount === 0) {
        var date = dateFormat(new Date());
        var values = [
          "Newsletter BEM MIPA UI",
          "Aisha Nurtabina",
          "BEM MIPA sedang naik daun",
          "https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg",
          `${date}`,
        ];
        let createArticle = `INSERT INTO ARTICLE(title, writer, content, thumbnail_photo, created_date) 
                VALUES ($1, $2, $3, $4, $5) returning *`;

        // let createArticle = `INSERT INTO ARTICLE(title, writer, content, thumbnail_photo, created_date)
        //         VALUES ('Newsletter BEM MIPA UI', 'Aisha Nurtabina', 'BEM MIPA sedang naik daun', 'https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg', '${date}');`;
        client.query(createArticle, values, function (error, results) {
          if (error) {
            console.log(error);
          }
        });
      }
    });
  });

  client.query(createAdkesmaTable, (err, results, fields) => {
    if (err) {
      throw err;
    }
    var val = [];
    var setValue = (value) => {
      val = value;
    };
    let query = `SELECT * FROM ADKESMA;`;
    client.query(query, (err, results) => {
      if (err) {
        throw err;
      }
      setValue(results);
      var string = JSON.stringify(val);
      var adkesma = JSON.parse(string);

      if (adkesma.rowCount === 0) {
        let createAdkesma = `INSERT INTO ADKESMA(caption, url)
                VALUES('SK Penetapan Mahasiswa Tugas Akhir yang Membayar Biaya Operasional Pendidikan',
                'https://www.instagram.com/bemfmipaui/');`;
        client.query(createAdkesma, (err, results) => {
          if (err) {
            console.log(err);
          }
        });
      }
    });
  });
});

module.exports = client;
