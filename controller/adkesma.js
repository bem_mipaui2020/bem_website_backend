var connection = require('../connection')
var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')

exports.createCaption = function(req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.json({
                message: "Invalid token!"
            })
        }
        else{
            var caption = req.body.caption
            var url = req.body.url
            var query = `insert into adkesma (caption, url) values ('${caption}', '${url}')`
             console.log(req.body)
            connection.query(query, (err, results) => {

                if(err){
                    throw err
                }
                else{
                    console.log(req.body)
                    console.log(caption)
                    res.send({
                        message: "New caption successfully posted",
                    })
                }
            })
        }
    }) 
}

exports.captionDetail = function (req, res) {
    var query = `select * from adkesma;`

    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "success",
                data: results.rows
            })
        }
    })
}

exports.deleteCaption = function (req, res) {
    var id = req.params.id
    var query = `delete from adkesma where id = '${id}';`
    console.log(id)
    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "caption successfully deleted!",
            })
        }
    })
}
