var connection = require('../connection')
const jwt = require('jsonwebtoken');
var dateFormat = require('dateformat');

exports.readArticle = function (req, res) {
    var query = 'select * from article'

    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "success",
                data: results.rows
            })
        }
    })
}

exports.detailArticle = function (req, res) {
    var id  = req.params.id
    var query = `select * from article where id_article = ${id}`

    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "success",
                data: results.rows
            })
        }
    })
}

exports.createArticle = function (req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.json({
                message: "Invalid token!",
                authData
            })
        }
        else{
            var title = req.body.title
            var writer = req.body.writer
            var content = req.body.content
            var thumbnail_photo = req.body.thumbnail_photo
            var created_date = dateFormat(new Date())
            var values = [`${title}`, `${writer}`, `${content}`, `${thumbnail_photo}`, `${created_date}`]
            var query = `INSERT INTO ARTICLE(title, writer, content, thumbnail_photo, created_date) 
                VALUES ($1, $2, $3, $4, $5) returning *`;
            console.log(content)
            connection.query(query, values, (err, results) => {
                
                if(err){
                    throw err
                }
                else{
                    res.send({
                        message: "New article successfully posted",
                    })
                }
            })
        }
    })
}

exports.deleteArticle = function (req, res) {
    var id = req.params.id
    var query = `delete from article where id_article = '${id}';`
    console.log(id)
    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "article successfully deleted!",
            })
        }
    })
}