var connection = require('../connection')
var bcrypt = require('bcryptjs')

exports.register = function(req, res) {
    var username = req.body.username
    var password = req.body.password
    var hashedPassword = bcrypt.hashSync(password);
    var query = `insert into users (username, password) values ('${username}', '${hashedPassword}')`

    connection.query(query, (err, results) => {
        if(err){
            throw err
        }
        else{
            res.send({
                message: "success",
                data: results.rows
            })
        }
    })
}
