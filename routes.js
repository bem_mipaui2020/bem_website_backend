
module.exports = (app) => {
    var auth = require('./auth')
    var authentication = require('./controller/authentication')
    var user = require('./controller/user')
    var article = require('./controller/article')
    var createUser = require('./controller/createUser')
    var adkesma = require('./controller/adkesma')
    const jwt = require('jsonwebtoken')

    // Get initial api
    app.get('/api', async (req,res) => {
        res.json({
            message: "Welcome to the BEM MIPA API"
        })
    })

    // Get token api
    app.route('/api/token')
        .post(authentication.token)

    // Post register api
    app.route('/api/register')
        .post(createUser.register)

    // Authentication
    app.route('/api/login')
        .post(authentication.login)

    app.route('/api/logout')
        .get(auth.verifyToken, authentication.logout)

    // Get users api
    app.route('/api/users')
        .get(user.readUsers)

    // Articles
    app.route('/api/article')
        .get(article.readArticle)

    app.route('/api/detail-article/:id')
        .get(article.detailArticle)

    app.route('/api/create-article')
        .post(article.createArticle)

    app.route('/api/delete-article/:id')
        .delete(article.deleteArticle)

    // Adkesma
    app.route('/api/adkesma')
        .get(auth.verifyToken, adkesma.captionDetail)
    
    app.route('/api/create-adkesma')
        .post(auth.verifyToken, adkesma.createCaption)
    
    app.route(`/api/delete-adkesma/:id`)
        .delete(auth.verifyToken, adkesma.deleteCaption)

    

    app.post('/api/post', auth.verifyToken, async (req, res) => {
        jwt.verify(req.token, 'secretkey', (err, authData) => {
            if(err){
                res.send('Forbidden')
            }
            res.json({
                message: "Post Created...",
                authData,

            })
        })
        
    })

    // app.post('/api/login', async (req, res) => {
    //     // Mock user
    //     const user = {
    //         id: 1,
    //         username: 'Bob',
    //         email: 'bob@gmail.com'
    //     }
    //     jwt.sign({user: user}, 'secretkey', {expiresIn: '30s'}, (err, token) => {
    //         res.json({
    //             token: token
    //         })
    //     })
    // })

    // Format of token
    // Authorization: Bearer <access_token>

    // Verify Token
    // function verifyToken(req, res, next) {
    //     // Get auth header value
    //     const bearerHeader = req.headers['authorization']
    //     // Check if bearer is not undefined
    //     if(typeof bearerHeader !== 'undefined'){
    //         // Split at the space
    //         const bearer = bearerHeader.split(" ")
    //         // Get token from array
    //         const bearerToken = bearer[1]
    //         // Set the token
    //         req.token = bearerToken;
    //         // Next Middleware
    //         next()
    //     }
    //     else{
    //         res.send({
    //             message: 'Forbidden'
    //         })
    //     }
    // }

    // get users api
    // app.get('/api/users', async (req, res) => {
    //     const rows = await user()
    //     res.send(JSON.stringify(rows))
    //     console.table(rows)
    // })

    // get article's api
    // app.get('/api/article', async (req, res) => {
    //     const rows = await article()
    //     res.send(JSON.stringify(rows))
    //     console.table(rows)
    // })

    // post users api
    // app.post('/api/users', async (req,res) => {
    //     let result = {}
    //     try{
    //         const reqJson = req.body
    //         await createUser(reqJson.username, reqJson.password)
    //         result.message = "success"
            
    //     }
    //     catch(e){
    //         result.message = "failed to post an api, try again"
    //     }
    //     finally{
    //         res.setHeader("content-type", "application/json")
    //         res.send(JSON.stringify(result))
    //         const rows = await user()
    //         console.table(rows)
    //     }
    // })

    
    
}